import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put } from '@nestjs/common';
import { CreatePostDto } from './dto/create-post.dto';
import { Post as BlogPost } from './interfaces/post.interface';
import { PostsService } from './posts.service';

@Controller('posts')
export class PostsController {

    constructor(
        private postsService: PostsService,
    ) {}

    @Get(':id')
    @HttpCode(200)
    getOne(@Param('id') id: string): BlogPost {
        return this.postsService.findOne(Number(id));
    }

    @Get()
    @HttpCode(200)
    getAll(): BlogPost[] {
        return this.postsService.findAll();
    }

    @Post()
    @HttpCode(201)
    create(@Body() createPostDto: CreatePostDto): BlogPost {
        return this.postsService.create(createPostDto);
    }

    @Delete(':id')
    @HttpCode(200)
    deleteOne(@Param('id') id: string): void {

        this.postsService.deleteOne(Number(id));
    }

    @Delete()
    @HttpCode(200)
    deleteAll(): void {
        this.postsService.deleteAll();
    }

    @Put(':id')
    @HttpCode(200)
    update(@Param('id') id: string, @Body() createPostDto: CreatePostDto) {
        console.log(id);
        this.postsService.update(Number(id), createPostDto);
    }

}
