import { Post } from './../interfaces/post.interface';
export class CreatePostDto implements Post {
    readonly title: string;
    readonly content: string;
    readonly author: string;

    readonly tags?: string[];

}
