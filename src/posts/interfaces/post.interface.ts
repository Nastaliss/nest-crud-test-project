export interface Post {
    title: string;
    content: string;
    author: string;

    tags?: string[];
    id?: number;
}
