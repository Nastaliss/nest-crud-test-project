import * as mongoose from 'mongoose';

export const PostSchema = new mongoose.Schema({
    title: String,
    content: Number,
    author: String,
    tags: Array,
});