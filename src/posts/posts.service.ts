import { Injectable } from '@nestjs/common';
import { Post } from './interfaces/post.interface';

@Injectable()
export class PostsService {

    private posts: Post[] = [];

    public create(post: Post): Post {
        this.posts.push(post);
        return post;
    }

    public findOne(id: number): Post {
        for (const post of this.posts) {
            if (id === post.id) {
                return post;
            }
        }
        throw new Error('Post not found');
    }

    public findAll(): Post[] {
        return this.posts;
    }

    public deleteAll(): void {
        this.posts = [];
    }

    public deleteOne(id: number) {
        this.posts = this.posts.filter((post: Post) => {
            return id !== post.id;
        });
    }

    public update(id: number, post: Post): Post {
        let editedPost = this.findOne(id);
        editedPost = Object.assign(editedPost, post);
        return editedPost;
    }
}
