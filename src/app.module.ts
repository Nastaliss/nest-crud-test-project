import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { DATABASE_LOGIN, DATABASE_PASSWORD } from 'keys';
import { PostsController } from './posts/posts.controller';
import { PostsModule } from './posts/posts.module';
import { PostsService } from './posts/posts.service';

@Module({
  imports: [
      MongooseModule.forRoot(`mongodb+srv://${DATABASE_LOGIN}:${DATABASE_PASSWORD}@mongo-fswmj.mongodb.net/test?retryWrites=true`),
      PostsModule,
  ],
  controllers: [PostsController],
  providers: [PostsService],
})
export class AppModule {}
