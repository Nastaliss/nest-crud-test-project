export declare class PostsController {
    getParam(id: any): string;
    getQuery(query: Date): string;
    getAll(): string;
}
